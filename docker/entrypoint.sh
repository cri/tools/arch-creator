#!/bin/sh

IMAGE_NAME="${1}"
ENV_NAME="${2:-"base"}"

set -xeu

./arch-creator.sh build -e "${ENV_NAME}" "${IMAGE_NAME}"
./arch-creator.sh torrent "${IMAGE_NAME}"

DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

aws s3 --endpoint-url "${AWS_ENDPOINT_URL}" cp --recursive --acl public-read \
  "${PWD}/imgs" "s3://${AWS_PXE_IMAGES_BUCKET}" \
  --metadata "version=${DATE},comment=${DATE}"
